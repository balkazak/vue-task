import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    docNumber: '',
    valid: true,
    firstname: "",
    lastname: "",
    email: "",
    gender: null,
    genders: [
      'Мужской',
      'Женский'
    ],
    dayValue: '',
    monthValue: '',
    yearValue: '',
    nation: null,
    nations: [
      'Казахстан',
      'Америка',
      'Другое'
    ],
    docType: null,
    docTypes: [
      'Уд. Личности',
      "Паспорт"
    ],
    dayValid: '',
    monthValid: '',
    yearValid: '',
    price: null,
    fee: '',
  },
  mutations: {
    updateLastName: (state, lastname) => {
      state.lastname = lastname;
    },
    updateFirstName: (state, firstname) => {
      state.firstname = firstname;
    },
    updateGender: (state, gender) => {
      state.gender = gender;
    },
    updateDayValue: (state, dayValue) => {
      state.dayValue = dayValue;
    },
    updateMonthValue: (state, monthValue) => {
      state.monthValue = monthValue;
    },
    updateYearValue: (state, yearValue) => {
      state.yearValue = yearValue;
    },
    updateNation: (state, nation) => {
      state.nation = nation;
    },
    updateDocType: (state, docType) => {
      state.docType = docType;
    },
    updateDocNumber: (state, docNumber) => {
      state.docNumber = docNumber;
    },
    updateDayValid: (state, dayValid) => {
      state.dayValid = dayValid;
    },
    updateMonthValid: (state, monthValid) => {
      state.monthValid = monthValid;
    },
    updateYearValid: (state, yearValid) => {
      state.yearValid = yearValid;
    },
    updatePrice: (state, price) => {
      state.price = price;
    },
    updateFee: (state, fee) => {
      state.fee = fee;
    },
  },
  actions: {
  },

});
